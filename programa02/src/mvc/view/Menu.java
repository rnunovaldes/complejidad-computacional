package mvc.view;

//import utilidad.Escritor;
import utilidad.Printer;
import utilidad.MenuConsole;

import mvc.controller.WindowManager;

/**
 * Clase para mostrar menu con opciones en la consola
 */
public class Menu {

    private static final String[] OPTIONS =
    {/*0*/"¿Cuántos elementos quiere que tenga el conjunto?",
     /*1*/"¿Cuántos subconjuntos tendrá?",
     /*2*/"Escriba cuáles elementos (el nombre exacto o solo el número que les"
     + " asignó el programa separados por comas) forman parte del subconjuno #",
     /*3*/"¿Acepta este ejemplar para solucionarlo?",
     /*4*/"¿Quiere volver a usar el programa o salir?",
     /*5*/"Sí",/*6*/"No",/*7*/"Generar otro ejemplar",/*8*/"Salir",
     /*9*/"No existe una cubierta"};
    private MenuConsole cm = new MenuConsole();
    private WindowManager wm;

    public Menu(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que inicia el menu para imprimirse en consola
     */
    public void start() {
        boolean loop = true;
        while(loop) {
            setCovering();
            loop = cm.askDual(OPTIONS[4], OPTIONS[7], OPTIONS[8]);
        }
    }

    public void setCovering() {
        boolean loop = true;
        while(loop) {
            Printer.printWarning(OPTIONS[0]);
            int elements = cm.askInteger(false);
            Printer.println(wm.requestSet(elements));
            Printer.printWarning(OPTIONS[1]);
            int subsets = cm.askInteger(false);
            for(int i = 0; i < subsets; i++) {
                Printer.printWarning(OPTIONS[2] + String.valueOf(i));
                String input = cm.askString(true);
                Printer.printExtra(input);
                String[] subinput = input.split(",");
                wm.sendElementsForSubset(subinput,i);
            }
            Printer.println(wm.requestSetRepresentation());
            if(cm.askYesNo(OPTIONS[3])) {
                loop = false;
                if(!wm.solveSetCovering()){
                    Printer.printError(OPTIONS[9]);
                }
            }
        }
    }
}
