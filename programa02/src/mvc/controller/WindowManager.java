package mvc.controller;

import mvc.view.Menu;
import mvc.model.Model;

/**
 * Clase que coordina las ventanas y la lógica del proyecto
 */
public class WindowManager {
    private Menu menu;
    private Model model;

    /**
	 * Constructor de la clase
	 */
	public WindowManager() {
	    menu = new Menu(this);
		model = new Model(this);
        menu.start();
	}

    /**
     * Método para avisar al controlador que debe generar un conjunto nuevo
     *
     * @param elements La cantidad de elementos del conjunto
     * @return La representación en cadena del conjunto
     */
    public String requestSet(int elements) {
        return model.generateSet(elements);
    }

    /**
     * Método para enviar al controlador los elementos de un nuevo subconjunto
     * para su conjunto generado
     *
     * @param subinput Un arreglo de cadenas que representan el nombre de los
     *                 elementos a agrupar en un subconjunto nuevo
     */
    public void sendElementsForSubset(String[] subinput,int subset) {
        model.asignSubset(subinput,subset);
    }

    /**
     * Método que solicita una representación en cadena del conjunto y sus
     * subconjuntos
     *
     * @return La representación en cadena
     */
    public String requestSetRepresentation() {
        return model.representateSet();
    }

    /**
     * Método para avisar al controlador que solucione el problema con el
     * conjunto que tiene generado
     *
     * @return Si pudo solucionar el problema
     */
    public boolean solveSetCovering() {
        return model.solveSetCovering();
    }
}
