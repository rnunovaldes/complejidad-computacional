package mvc.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import utilidad.console.Color;

/**
 * Clase que modela un conjunto
 */
public class MySet implements Cloneable {

    private String label;
    private Set<String> set;
    private Map<Integer,MySet> subsets;
    private int size, index;

    /**
     * Constructor de la clase
     */
    public MySet() {
        label = "S";
        index = 0;
        set = new HashSet<String>();
        subsets = new HashMap<Integer,MySet>();
        size = 0;
    }

    /**
     * Constructor de la clase
     */
    private MySet(String label) {
        this.label = label;
        index = 0;
        set = new HashSet<String>();
        subsets = new HashMap<Integer,MySet>();
        size = 0;
    }

    /**
     * Constructor privado de la clase
     */
    private MySet(Set<String> set, Map<Integer,MySet> subsets,
                  int size, String label, int index) {
        this.label = label;
        this.index = index;
        this.set = set;
        this.subsets = subsets;
        this.size = size;
    }

    public Iterator<String> iterator() {
        return set.iterator();
    }

    /**
     * Método que comprueba si el conjunto es vacío
     *
     * @return True si no tiene elementos, False en caso contrario
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Obtiene la cantidad de elementos del conjunto
     *
     * @return Un entero que es el tamaño del conjunto
     */
    public int getSize() {
        return size;
    }

    /**
     * Método que comprueba si un elemento existe en el conjunto
     *
     * @param name El nombre del elemento
     * @return Un boleano que indica si el elemento esta o no en el conjunto
     */
    public boolean contains(String name) {
        return set.contains(name);
    }

    /**
     * Método que agrega un elemento al conjunto
     *
     * @param name El nombre con el que se identificará el elemento
     */
    public void addElement(String name) {
        if(set.add(name)) size++;
    }

    /**
     * Método que elimina un elemento del conjunto
     * @param name El nombre del elemento
     */
    public void removeElement(String name) {
        if(set.remove(name)) size--;
    }

    /**
     * Obtiene un arreglo de todos los subconjuntos definidos del conjunto
     *
     * @return Un arreglo de subconjuntos
     */
    public ArrayList<MySet> getSubsets() {
        ArrayList<MySet> mySubsets = new ArrayList<>();
        for (MySet mySet: subsets.values()) {
            try {
                mySubsets.add((MySet)mySet.clone());
            } catch(Exception e) {}
        }
        return mySubsets;
    }

    /**
     * Método que hace que un elemento del conjunto forme parte de un
     * subconjunto dado
     *
     * @param name El nombre del elemento
     * @param subset El numero que representa el subconjunto
     */
    public void addElementToSubset(String name, int subset) {
        if(set.contains(name)) {
            Integer keyref = Integer.valueOf(subset);
            if(!subsets.containsKey(keyref)){
                subsets.put(keyref,new MySet("s" + index));
                index++;
            }
            subsets.get(keyref).addElement(name);
        }
    }

    /**
     * Método que hace que un elemento del conjunto deje de fomrar parte de un
     * subconjunto dado
     * @param name El nombre del elemento
     * @param subset El número que representa el subconjunto
     */
    public void removeElementFromSubset(String name, int subset) {
        Integer keyref = Integer.valueOf(subset);
        if(subsets.containsKey(keyref)) {
            subsets.get(keyref).removeElement(name);
            if(subsets.get(keyref).isEmpty()) subsets.remove(keyref);
        }
    }

    /**
     * Calcula la unión de dos conjuntos
     *
     * @param El conjunto que se quiere unir
     *        (ie. Este conjunto U Otro conjunto)
     * @return Un nuevo conjunto que es la unión de ambos
     */
    public MySet union(MySet otherSet) {
        MySet newSet = new MySet();
        try {
            newSet = (MySet)this.clone();
        } catch(Exception e) {}
        Iterator<String> it = otherSet.iterator();
        while(it.hasNext()) {
            newSet.addElement(it.next());
        }
        return newSet;
    }

    /**
     * Calcula la intersección de dos conjuntos
     *
     * @param El conjunto que se quiere intersecar
     *        (ie. Este conjunto U Otro conjunto)
     * @return Un nuevo conjunto que es la intersección de ambos
     */
    public MySet intersection(MySet otherSet) {
        MySet newSet = new MySet();
        Iterator<String> it = otherSet.iterator();
        while(it.hasNext()) {
            String name = it.next();
            if(set.contains(name)) newSet.addElement(name);
        }
        return newSet;
    }

    /**
     * Calcula la diferencia de dos conjuntos
     *
     * @param El conjunto que tiene los elementos a retirar
     *        (ie. Este conjunto \ Otro conjunto)
     * @return Un nuevo conjunto que es la diferencia de ambos
     */
    public MySet difference(MySet otherSet) {
        MySet newSet = new MySet();
        try {
            newSet = (MySet)this.clone();
        } catch(Exception e) {}
        Iterator<String> it = otherSet.iterator();
        while(it.hasNext()) {
            String name = it.next();
            if(set.contains(name)) newSet.removeElement(name);
        }
        return newSet;
    }

    /**
     * Método que clona el conjunto (una copia profunda)
     *
     * @return Un objeto que puede convertirse en Set y es igual al conjunto
     *         que usó el método
     * @throw No arroja excepciones
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        Map<Integer, MySet> clonedSubset = new HashMap<>();
        if(!subsets.isEmpty()) {
            for (Map.Entry<Integer, MySet> entry: subsets.entrySet()) {
                try {
                    MySet clonedMySet = (MySet) entry.getValue().clone();
                    clonedSubset.put(entry.getKey(), clonedMySet);
                } catch(Exception e) {}
            }
        }
        return new MySet(new HashSet<>(set),clonedSubset,size,label,index);
    }

    /**
     * Regresa una representación en cadena del conjunto
     * @return Una representación
     */
    @Override
    public String toString() {
        Color[] normalColors = new Color[7];
        System.arraycopy(Color.values(),26,normalColors,0,7);
        Color[] brightColors = new Color[8];
        System.arraycopy(Color.values(),49,brightColors,0,8);
        Color[] colors = new Color[15];
        System.arraycopy(normalColors, 0, colors, 0, 7);
        System.arraycopy(brightColors, 0, colors, 7, 8);

        String str = String.format("%-5s\t", label);
        //String str = "| ";
        for(String name: set) {
            str += String.format("%-5s\t", name);
            //str += name + " | ";
        }
        if(subsets.size() > 0) str += "\n";
        Integer[] keys = subsets.keySet().toArray(new Integer[subsets.size()]);
        for(int i = 0; i < subsets.size(); i++) {
            str += String.format("%-5s\t", subsets.get(keys[i]).label);
            for(String name: set) {
                String spaces = " ".repeat(name.length());
                if(subsets.get(keys[i]).contains(name)) {
                    str += colors[i % colors.length]
                        + String.format("%-5s\t"," "+spaces+" ")
                        + Color.RESET;
                    //str += colors[i] + " " + spaces + " " + Color.RESET + " ";
                } else {
                    str += String.format("%-5s\t","  "+spaces);
                    //str += spaces + "   ";
                }
            }
            str += (i+1 != subsets.size())? "\n": "";
        }
        return str;
    }
}
