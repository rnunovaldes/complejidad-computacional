package mvc.model;

import java.util.ArrayList;

import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir ejemplares de los problemas y sus soluciones
 */
public class Model {
    private WindowManager wm;
    private MySet generatedSet;

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las ventanas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que genera un conjunto
     *
     * @param elements La cantidad de elementos del conjunto a generar
     * @return Una representación del conjunto en cadena
     */
    public String generateSet(int elements){
        generatedSet = new MySet();
        for(int i = 0; i< elements; i++) {
            String e = "e" + String.valueOf(i);
            generatedSet.addElement(e);
        }
        return generatedSet.toString();
    }

    /**
     * Método que asigna elementos a un nuevo subconjunto para el conjunto
     *
     * @param subinput Un arreglo de cadenas que representan el nombre de los
     *                 elementos a agrupar en un subconjunto nuevo
     */
    public void asignSubset(String[] subinput, int subset){
        for(String element: subinput) {
            try {
                int number = Integer.parseInt(element);
                generatedSet.addElementToSubset("e"+number,subset);
            } catch (NumberFormatException e) {
                generatedSet.addElementToSubset(element.trim(),subset);
            }
        }
    }

    /**
     * Método que da la representación en cadena del conjunto y sus subconjuntos
     *
     * @return La representación en cadena
     */
    public String representateSet() {
        return generatedSet.toString();
    }

    public boolean solveSetCovering() {
        ArrayList<MySet> solution = new ArrayList<>();
        MySet u = new MySet();
        try {
            u = (MySet)generatedSet.clone();
        } catch(Exception e) {}
        MySet c = new MySet();
        ArrayList<MySet> f = generatedSet.getSubsets();
        while(!u.isEmpty() && !f.isEmpty()) {
            MySet biggest = new MySet();
            for(MySet s :f) {
                MySet intersection = s.intersection(u);
                if(biggest.getSize() <= intersection.getSize()) biggest = s;
            }
            f.remove(biggest);
            solution.add(biggest);
            u = u.difference(biggest);
            c = c.union(biggest);
        }
        if(c.getSize() == generatedSet.getSize()) {
            System.out.println("Estos subconjuntos solucionan el problema:");
            for(MySet sets: solution) {
                System.out.println(sets.toString());
            }
            return true;
        }
        return false;
    }
}
