@ECHO off
@chcp 65001>nul

SET "EXEC=main.Principal"
SET "JAVA_PRIN=src\main\Principal.java"
SET "CLAS_PRIN=bin\main\Principal.class"
SET "CLAS_UTIL1=bin\utilidad\Reader.class"
SET "CLAS_UTIL2=bin\utilidad\Printer.class"
SET "CLAS_UTIL3=bin\utilidad\StringValidator.class"
SET "CLAS_UTIL4=bin\utilidad\MenuConsole.class"
SET "CLAS_UTIL5=bin\utilidad\console\SpecialCharacter.class"
SET "CLAS_UTIL6=bin\utilidad\console\Color.class"
SET "CLAS_VIEW=bin\mvc\view\Menu.class"
SET "CLAS_MOD1=bin\mvc\model\Set.class"
SET "CLAS_MOD2=bin\mvc\model\Model.class"

::FORFILES /M %~dp0 /S /C "cmd /c IF EXIST Principal.class GOTO EXE"

:UseChoice
choice /c SN /n /m "¿Compilar (S/N)?"
IF ERRORLEVEL == 2 goto CON
if ERRORLEVEL == 1 goto COM

:CON
IF EXIST %CLAS_PRIN% IF EXIST %CLAS_UTIL1% IF EXIST %CLAS_UTIL2% (
IF EXIST %CLAS_UTIL3% IF EXIST %CLAS_UTIL4% IF EXIST %CLAS_UTIL5% (
IF EXIST %CLAS_UTIL6% IF EXIST %CLAS_VIEW% IF EXIST %CLAS_MOD1% (
IF EXIST %CLAS_MOD2% (
GOTO EXE
))))

:COM
IF NOT EXIST bin MKDIR bin
ECHO Vamos a compilar
javac -encoding UTF-8 -d bin -cp ..;src %JAVA_PRIN%

:EXE
ECHO Vamos a ejecutar el compilado
java -cp bin %EXEC%
PAUSE
ECHO Se eliminara la carpeta con los class
RMDIR /s bin
EXIT
