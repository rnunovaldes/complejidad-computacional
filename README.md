# Complejidad Computacional - Repositorio

Repositorio que tiene programas en java encargados como prácticas durante el
curso de Complejidad computacional de la Facultad de Ciencias, UNAM

## Uso del los progrmas
Cada práctica encargada tienen dos scripts llamados `program`, uno es `.sh` y
el otro `.bat` para que pueda ejecutarse el programa java tanto en Unix como
Windows. Solo hay que ejecutar el correspondiente script.

Se trabajó con java11 (pero no se usaron paquetes particulares, así que java8
seguro funciona).

## Organización de directorios
```
/
├── programa01/
│   ├── src/
│   │   ├── main
│   │   │   └── Pricipal.java
│   │   └── ...
│   ├── program.sh
│   └── program.bat
├── programa0X/
│   └── ...
└── utilidad/
    └── ...
```

En "utilidad" están varias clases útilies para los programas que son de uso
general.

En "programa0X" está cada una de las prácticas encargadas, probalemente junto a
un pdf que explica en cierto grado de que trata la misma. Además están los
scripts para ejecutar el programa. En "src" están las clases necesarias para
los programas, en particular se hizo uso de enlaces simbólicos hacia
`/utilidad` (Virtudes de usar Linux 😎️)

## Autor
- Raúl N. Valdés
