package main;

import java.io.IOException;
import java.io.File;

import mvc.controller.WindowManager;

/**
 * Clase que inicia la ejecucion del proyecto
 */
public class Principal {

  public static final String sep = System.getProperty("file.separator");

	/**
	 * Metodo principal
	 */
	public static void main(String[] args) {
    WindowManager wm = new WindowManager();
  }
}
