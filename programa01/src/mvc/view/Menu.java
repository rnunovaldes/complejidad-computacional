package mvc.view;

import utilidad.MenuConsole;
import utilidad.Printer;
import utilidad.console.SpecialCharacter;

import mvc.controller.WindowManager;

/**
 * Clase para mostrar menu con opciones en la consola
 */
public class Menu {

    private static final String[] options =
    {/*0*/"¿Qué problema se resolverá?",
    /*1*/"Resolver problema de la alcanzabilidad",/*2*/"Resolver problema 3SAT",
    /*3*/"Generar Ejemplar",/*4*/"Otro",/*5*/"Resolver",
    /*6*/"¿Cuántas aristas quiere que haya?",
    /*7*/"¿Salir?",/*8*/"Sí",/*9*/"No",
    /*10*/"Indique el vértice inicial (Asegurese de escribir bien el nombre u "
    + "obtendrá una linda excepción): ",/*11*/"Indique el vértice final (misma "
    + "advertencia): ", /*12*/ "Generar otro o resolver problema con este "
    + "ejemplar"};
    private MenuConsole cm = new MenuConsole();

    private WindowManager wm;

    public Menu(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que inicia el menú para imprimirse en consola
     */
    public void start() {
        boolean loop = true;
        while(loop) {
            if(cm.askDual(options[0], options[1], options[2])) {
                /*ALCANZABILIDAD*/
                reachability();
            } else {
                /*3SAT*/
                satisfiability();
            }
            if(cm.askYesNo(options[7])) loop = false;
            Printer.println();
        }
    }

    /**
     * Método que continene el menú para el problema de alcanzabilidad
     */
    private void reachability() {
        boolean loop = true;
        while(loop) {
            Printer.println();
            Printer.printExtra(options[3]);
            Printer.println(options[6]);
            int edges = cm.askInteger(false);
            Printer.println(wm.requestGraph(edges));
            if(!cm.askDual(options[12],options[4],options[5])) {
                loop = false;
                Printer.println(options[10]);
                String vi = cm.askWord(true);
                Printer.println(options[11]);
                String vf = cm.askWord(true);
                if(wm.solveGraph(vi,vf)){
                    Printer.printOk("Hay un camino");
                } else {
                    Printer.printError("No existe un camino");
                }
            }
        }
    }

    /**
     * Método que continene el menú para el problema 3SAT
     */
    private void satisfiability() {
        boolean loop = true;
        while(loop) {
            Printer.println();
            Printer.printExtra(options[3]);
            Printer.println(wm.requestExpresion());
            if(!cm.askDual(options[12],options[4],options[5])) {
                loop = false;
                if(wm.solveExpresion()){
                    Printer.printOk("Satisfacible");
                } else {
                    Printer.printError("Insatisfacible");
                }
            }
        }
    }
}
