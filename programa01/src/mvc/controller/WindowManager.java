package mvc.controller;

import mvc.view.Menu;
import mvc.model.Model;

/**
 * Clase que coordina las vistas y la lógica del proyecto
 */
public class WindowManager {

    private Menu menu;
    private Model model;

    /**
     * Constructor de la clase
     */
    public WindowManager() {
        menu = new Menu(this);
        model = new Model(this);
        menu.start();
    }

    /**
     * Método para avisar al controlador que debe generar una gráfica nueva
     *
     * @param edges Cuántas aristas tendra la grafica
     * @return Una representación en cadena de la expresión
     */
    public String requestGraph(int edges) {
        return model.generateGraph(edges);
    }

    /**
     * Método para avisar al controlador que debe resolver el problema de
     * alcanzabilidad para la última gráfica que se generó
     *
     * @return Si es posible resolverlo
     */
    public boolean solveGraph(String vi, String vf) {
        return model.solveGraph(vi,vf);
    }

    /**
     * Método para avisar al controlador que debe generar una expresión nueva
     *
     * @return Una representación en cadena de la expresión
     */
    public String requestExpresion() {
        return model.generateExpresion();
    }

    /**
     * Método para avisar al controlador que debe resolver el problema 3SAT
     * para la última expresión que se generó
     *
     * @return Si es posible resolverlo
     */
    public boolean solveExpresion() {
        return model.solveExpresion();
    }
}
