package mvc.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Clase que modela una gráfica con varios vértices y aristas
 */
public class Graph implements Cloneable {
    private Map<String, List<String>> neighbors;
    private int vertex_number;

    /**
     * Constructor de la clase vacío
     */
    public Graph() {
        neighbors = new HashMap<String, List<String>>();
        vertex_number = 0;
    }

    /**
     * Constructor privado usado para clonar una gráfica
     */
    private Graph(Map<String, List<String>> neighbors, int vertex_number) {
        this.neighbors = neighbors;
        this.vertex_number = vertex_number;
    }

    /**
     * Método que crea un vértice en la gráfica con un nombre (de preferencia
     * único)
     *
     * @param name Una cadena identificadora para el vértice
     */
    void addVertex(String name) {
        neighbors.putIfAbsent(name, new ArrayList<String>());
        vertex_number++;
    }

    /**
     * Metodo que elimina un vertice de la grafica
     *
     * @param name Una cadena que representa el nombre identificador del vértice
     */
    void removeVertex(String name) {
        neighbors.values().stream().forEach(e -> e.remove(name));
        neighbors.remove(name);
        vertex_number--;
    }

    /**
     * Método que agrega una arista a la gréfica dados dos nombres para vértices
     * Los vértices a usar serán la primera ocurrencia
     *
     * @param name1 El identificador del primer vértice
     * @param name2 El identificador del segundo vértice
     */
    void addEdge(String name1, String name2) {
        neighbors.get(name1).add(name2);
        neighbors.get(name2).add(name1);
    }

    /**
     * Método que elimina las aristas de la gréfica que forman un camino entre
     * dos vértices dados
     *
     * @param name1 El identificador del primer vértice
     * @param name2 El identificador del segundo vértice
     */
    void removeEdge(String name1, String name2) {
        List<String> eV1 = neighbors.get(name1);
        List<String> eV2 = neighbors.get(name1);
        if (eV1 != null) eV1.remove(name2);
        if (eV2 != null) eV2.remove(name1);
    }

    /**
     * Método para obtener los vertices vecinos de un vértice dado
     *
     * @param name El identificador del vértice
     */
    public List<String> getAdjVertices(String name) {
        return neighbors.get(name);
    }

    /**
     * Método que obtiene la cantidad de vértices actuales en la gráfica
     */
    public int getVertexNumber() {
        return this.vertex_number;
    }

    /**
     * Método que clona la gráfica
     *
     * @return Un objeto que puede convertirse en Graph y es igual a la gráfica
     * que uso el método clone
     */
    public Object clone() throws CloneNotSupportedException {
        try {
            Map<String, List<String>> shallowCopy =
                    new HashMap<String, List<String>>();
            Set<Map.Entry<String, List<String>>> entries = neighbors.entrySet();
            for (Map.Entry<String, List<String>> mapEntry : entries) {
                shallowCopy.put(mapEntry.getKey(),
                        (List<String>) ((ArrayList<String>) mapEntry.getValue())
                                .clone());
            }
            return (Object) new Graph(shallowCopy, vertex_number);
        } catch (Exception e) {}
        throw new CloneNotSupportedException();
    }

    /**
     * Método que hace una representación de la gráfica en una cadena
     *
     * @return La representación
     */
    @Override
    public String toString() {
        Set<String> vertexes = neighbors.keySet();
        String str = "";
        for (String v : vertexes) {
            List<String> vneighbors = neighbors.get(v);
            String vstr = "";
            for (String vn : vneighbors) {
                vstr += vn.toString() + " ";
            }
            str += v.toString() + " -> [" + vstr + "]\n";
        }
        return str;
    }
}
