package mvc.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Clase que se encarga de generar graficas resolver el problema de
 * alcanzabilidad
 */
public class ModelG {
    private Graph graph;
    private Random random = new Random();
    private final int not = 10;
    private String[] posible;

    /**
     * Método que genera una grafica de manera aleatorea. Escoje entre 10 o 20
     * para crear vertices y asigna aristas que se indiquen escogiendo entre los
     * dos numeros mas altos que se le asignen a los vertices
     *
     * @param edges La cantidad de aristas que se le va a asignar
     * @return La representación en cadena de la gráfica generada
     */
    public String generate(int edges) {
        graph = new Graph();
        int vertex = random.nextInt(11) + 10;
        // Crea los vértices
        for (int i = 0; i < vertex; i++) {
            String v = "v" + Integer.toString(i);
            graph.addVertex(v);
        }
        // Probabilidad de ser escogido
        int[][] probChosen = new int[vertex][2];
        // Cuantas veces se ha escogido
        int[][] timesChosen = new int[vertex][2];
        // Para cada vertice indicado
        for (int i = 0; i < edges; i++) {
            // Darle una probabilidad a cada vertice
            for (int j = 0; j < vertex; j++) {
                //(int)Math.floor(Math.random() * (max - min + 1) + min);
                // rand.nextInt((max - min) + 1) + min;
                probChosen[j][0] =
                        random.nextInt(101) - (timesChosen[j][0] * not);
                probChosen[j][1] =
                        random.nextInt(101) - (timesChosen[j][1] * not);
            }
            int[][] biggest = {{-1, -1}, {-2147483648, -2147483648}};
            // Escoger el que tenga la mayor probabilidad (mayor random)
            for (int j = 0; j < vertex; j++) {
                if (probChosen[j][0] > biggest[0][1]) {
                    biggest[0][0] = j;
                    biggest[0][1] = probChosen[j][0];
                }
                if (probChosen[j][1] > biggest[1][1]) {
                    biggest[1][0] = j;
                    biggest[1][1] = probChosen[j][1];
                }
            }
            if (biggest[0][0] == biggest[1][0]) {
            }
            graph.addEdge("v" + biggest[0][0], "v" + biggest[1][0]);
            // Aumentar las veces escogido y disminuir sus futuras
            // probabilidades
            timesChosen[biggest[0][0]][0] = timesChosen[biggest[0][0]][0]++;
            timesChosen[biggest[1][0]][1] = timesChosen[biggest[1][0]][1]++;
        }
        return graph.toString();
    }

    /**
     * Empieza a buscar si hay solucion para el problema
     */
    public boolean solve(String start, String end) {
        ArrayList<String> way = new ArrayList();
        System.out.println();
        return solver(start, end, graph);
    }

    /**
     * Método que revisa si hay solucion inmediata (vel vertice inicial al final
     * en solo un paso) y si no, prueba todos los distintos caminos posibles
     *
     * @param start El nombre del vertice inicial
     * @param end El nombre del vertice final
     * @param graph La grafica donde se tiene que vusacr el camino
     * @return Si existe un camino de start a end
     */
    private boolean solver(String start, String end, Graph newGraph) {
        // Una lista con los vecinos al vertice inicial
        List<String> nv = newGraph.getAdjVertices(start);
        // El vertice no tiene vecinos, ya no podemos continual buscando
        if (nv == null) return false;
        // Tiene un ciclo consigo mismo, eliminar
        /*for(String self: nv){
          if(self.equals(start)) newGraph.removeEdge(start,start);
        }*/
        // Este vertice tiene como vecino al final, tiene camino
        if (nv.contains(end)) {
            System.out.print(end.toString() + "\t" + start.toString() + "\t");
            return true;
        } else {
            // No ha camino inmediato al final. Probar proximos caminos
            try {
                /*   Fase adivinadora   */
                /* Se propone un camino */
                Graph g = (Graph) newGraph.clone();
                g.removeVertex(start);
                // Para todos los posibles caminos (los vecinos del inicial)
                for (String posible_vertex : nv) {
                    /*       Fase verificadora      */
                    /* Se ve si se llega a el final */
                    if (solver(posible_vertex, end, g)) {
                        System.out.print(start.toString() + "\t");
                        return true;
                    }
                }
            } catch (CloneNotSupportedException e) {
            }
        }
        return false;
    }
}
