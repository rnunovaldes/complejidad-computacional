package mvc.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Clase que se encarga de generar una estructura de CNF de 5 clausulas con 3
 * variables
 */
public class Expresion implements Cloneable {

    /**
     * Modela una variable lógica
     */
    protected class Variable implements Cloneable {
        public boolean isPositive, value;
        public String label;

        /**
         * Contructor de la clase
         *
         * @param isPositive Si debe tratarse el valor de la variable como
         *                   complemento o no
         * @param label El nombre de la variable
         */
        public Variable(boolean isPositive, String label) {
            this.label = label;
            this.isPositive = isPositive;
        }

        /**
         * Método que asigna un valor de verdad a la variable
         *
         * @param value El valor de verdad
         */
        public void setValue(boolean value) {
            this.value = value;
        }

        /**
         * Indica si esta variable se evalua como verdad o falso. No usar si no
         * se ha asignado usado setValue
         *
         * @return El valor de verdad de la variable
         */
        public boolean solve() {
            return (isPositive) ? this.value : !this.value;
        }

        /**
         * Clona la variable
         */
        @Override
        public Object clone() throws CloneNotSupportedException {
            Variable v = new Variable(this.isPositive, this.label);
            v.setValue(this.value);
            return v;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) return false;
            if (o == this) return true;
            if (o instanceof Variable) {
                return this.label == ((Variable) o).label;
            }
            return false;
        }

        /**
         * Hace una representación en cadena de la variable. Pone el churrito si
         * su valor de verdad debe evaluarse como complemento
         */
        @Override
        public String toString() {
            return (this.isPositive) ? this.label : "~" + this.label;
        }
    }

    /**
     * Clase que modela una Clausula de 3 variables
     */
    protected class Clause implements Cloneable {
        public Variable v1, v2, v3;

        public Clause(Variable v1, Variable v2, Variable v3) {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }

        public void setValues(boolean v1, boolean v2, boolean v3) {
            this.v1.setValue(v1);
            this.v2.setValue(v2);
            this.v3.setValue(v3);
        }

        public boolean solve() {
            return (this.v1.solve() || this.v2.solve() || this.v3.solve());
        }

        /**
         * Clona a la clausula
         */
        @Override
        public Object clone() throws CloneNotSupportedException {
            return new Clause((Variable) this.v1.clone(),
                    (Variable) this.v2.clone(), (Variable) this.v3.clone());
        }

        @Override
        public String toString() {
            return "(" + v1.toString() + " v " + v2.toString() + " v "
                    + v3.toString() + ")";
        }
    }

    private int currentClauses;
    private Clause[] clauses = new Clause[5];
    private String[] variables = new String[10];

    /**
     * Constructor de la clase vacío
     */
    public Expresion() {
        currentClauses = 0;
    }

    /**
     * Contructor privado. Usado para clonar la clase
     */
    private Expresion(Clause c1, Clause c2, Clause c3, Clause c4, Clause c5) {
        this.clauses[0] = c1;
        this.clauses[1] = c2;
        this.clauses[2] = c3;
        this.clauses[3] = c4;
        this.clauses[4] = c5;
    }

    /**
     * Método que agrega una clausula nueva a la expresión. La expresión solo
     * puede tener a lo mas 5 clausulas. Se le indica si los valores de las
     * 3 variables seran tratados como complemento o no y el nombre de las 3
     * variables
     */
    public boolean addClause(boolean b1, String l1, boolean b2, String l2,
            boolean b3, String l3) {
        if (currentClauses == 5) return false;
        this.clauses[currentClauses] = new Clause(new Variable(b1, l1),
                new Variable(b2, l2), new Variable(b3, l3));
        currentClauses++;
        for (int i = 0; i < variables.length; i++) {
            if (variables[i] != null && variables[i].equals(l1)) break;
            if (variables[i] == null) {
                variables[i] = l1;
                break;
            }
        }
        for (int i = 0; i < variables.length; i++) {
            if (variables[i] != null && variables[i].equals(l2)) break;
            if (variables[i] == null) {
                variables[i] = l2;
                break;
            }
        }
        for (int i = 0; i < variables.length; i++) {
            if (variables[i] != null && variables[i].equals(l3)) break;
            if (variables[i] == null) {
                variables[i] = l3;
                break;
            }
        }
        return true;
    }

    /**
     * Método que asigna valores de verdad a una variable en específico de la
     * expresión
     */
    public boolean setValueTo(String variable, boolean value) {
        for (String l : variables) {
            if (l != null && l.equals(variable)) {
                for (Clause c : clauses) {
                    if (c.v1.label.equals(variable)) c.v1.value = value;
                    if (c.v2.label.equals(variable)) c.v2.value = value;
                    if (c.v3.label.equals(variable)) c.v3.value = value;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Indica el valor de verdad de la expresión
     */
    public boolean solve() {
        return (clauses[0].solve() && clauses[1].solve() && clauses[2].solve()
                && clauses[3].solve() && clauses[4].solve());
    }

    /**
     * Clona la expresión
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        Expresion exp = new Expresion((Clause) this.clauses[0].clone(),
                (Clause) this.clauses[1].clone(),
                (Clause) this.clauses[2].clone(),
                (Clause) this.clauses[3].clone(),
                (Clause) this.clauses[4].clone());
        exp.currentClauses = this.currentClauses;
        System.arraycopy(
                this.variables, 0, exp.variables, 0, this.variables.length);
        return exp;
    }

    /**
     * Método que hace una representación de la expresión
     */
    @Override
    public String toString() {
        if (currentClauses != 5) return "La expresión no está completa";
        String str = "[ ";
        for (int i = 0; i < this.clauses.length; i++) {
            if (i == 4) {
                str += this.clauses[i].toString() + " ]";
            } else {
                str += this.clauses[i].toString() + " ^ ";
            }
        }
        for (String l : variables) {
            System.out.print(l + ",");
        }
        System.out.println();
        return str;
    }
}
