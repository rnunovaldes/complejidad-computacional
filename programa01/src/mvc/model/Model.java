package mvc.model;

import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir ejemplares de los problemas y sus soluciones
 */
public class Model {
    private WindowManager wm;
    private ModelG mg = new ModelG();
    private ModelE me = new ModelE();

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las vistas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que genera una gréfica
     *
     * @param edges La cantidad de aristas que se le va a asignar
     * @return Una representacion de la grafica en cadena
     */
    public String generateGraph(int edges) {
        return mg.generate(edges);
    }

    /**
     * Método que genera una expresién
     *
     * @return Una representación en cadena de la expresión
     */
    public String generateExpresion() {
        return me.generate();
    }

    /**
     * Indica al modelo correspondiente que obtenga si hay un camino de un
     * vertice inicial a uno final sin repetir vertices
     *
     * @param start El vertice inicial
     * @param end El vertice final
     * @return Si exite el camino
     */
    public boolean solveGraph(String start, String end) {
        return mg.solve(start, end);
    }

    /**
     * Indica al modelo correspondiente que obtenga si es posible satisfacer la
     * ultima expresion generada
     *
     * @return Si es posible satisfacerla
     */
    public boolean solveExpresion() {
        return me.solve();
    }
}
