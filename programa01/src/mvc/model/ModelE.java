package mvc.model;

import java.util.Random;

/**
 * Clase que se encarga de generar expresiones y resolver el problema 3SAT
 */
public class ModelE {
    /**
     * Una clase que modela una lista circular muy basica que solo guarda ints
     */
    protected class Circular {
        private int[] array;
        private int size, currentsize, pivot;

        /**
         * Contructor de la lista circular básica
         *
         * @param size El tamaño de la lista
         */
        public Circular(int size) {
            array = new int[size];
            this.size = size;
            currentsize = 0;
            pivot = 0;
        }

        /**
         * Método que agrega un elemento donde haya espacio o remplaza al
         * elemento más antiguo
         *
         * @param something El elemento a agregar
         */
        public void add(int something) {
            if(currentsize < size) {
                array[currentsize] = something;
                currentsize++;
                pivot = (pivot + 1) % size;
            } else {
                array[pivot] = something;
                pivot = (pivot + 1) % size;
            }
        }

        /**
         * Método que indica si se contiene al elemento en la lista
         *
         * @param something El elemento a buscar
         * @return Si contiene o no al elemento
         */
        public boolean contains(int something) {
            for(int el: array) if(something == el) return true;
            return false;
        }

        /**
         * Vuelve la lista circular en un arreglo
         *
         * @return Un arreglo que tiene los elementos de la lista circular
         */
        public int[] toArray() {
            int[] array = new int[size];
            System.arraycopy(this.array, 0, array, 0, size);
            return array;
        }
    }

    private Expresion expresion;
    private Random random = new Random();
    private final String[] varnames = {"Z","Y","X","W","V","U","T","S","R","Q"};
    private final int combinations = (int)java.lang.Math.pow((double)2,(double)varnames.length);
    private boolean[][] posible = new boolean[varnames.length][combinations];

    /**
     * Contructor de la clase que genera todas las posibles soluciones del
     * problema 3SAT con al menos 10 variables
     */
    public ModelE() {
        generatePosibleSolutions();
    }

    /**
     * Método que genera una expresión de manera aleatoria
     *
     * @return Una representación en cadena de la expresión
     */
    public String generate() {
        expresion = new Expresion();
        int[] elected = {-1,-1,-1};
        /* Se usa la lista para no repetir los ultimos 7
	       variables ya usadas y tratar de dar mas "varidedad" */
        Circular lastElected = new Circular(7);
        for(int i = 0; i < 5; i++) {
            //(int)Math.floor(Math.random() * (max - min + 1) + min);
            //rand.nextInt((max - min) + 1) + min;
            /* Escoger nommbres de las variables de forma aleatoria */
            while(true) {
                elected[0] = random.nextInt(varnames.length);
                if(!lastElected.contains(elected[0])) break;
            }
            while(true) {
                elected[1] = random.nextInt(varnames.length);
                if(!lastElected.contains(elected[1])) break;
            }
            while(true) {
                elected[2] = random.nextInt(varnames.length);
                if(!lastElected.contains(elected[2])) break;
            }
            /* Evitar que vuelvan a salir los ultimos 3 */
            lastElected.add(elected[0]);
            lastElected.add(elected[1]);
            lastElected.add(elected[2]);
            /* Asigna valores de verdad aleatorios */
            expresion.addClause(random.nextBoolean(),varnames[elected[0]],
	                            random.nextBoolean(),varnames[elected[1]],
	                            random.nextBoolean(),varnames[elected[2]]);
        }
        return expresion.toString();
    }

    /* Fase adivinadora */
    /**
     * Método que genera todas los posibles valores para 10 variables
     */
    private void generatePosibleSolutions() {
        int i = 0;
        for(int z = 0; z < 2; z++) {
            posible[0][i] = (z == 0)? true: false;
            for(int y = 0; y < 2; y++) {
                posible[1][i] = (y == 0)? true: false;
                for(int x = 0; x < 2; x++) {
                    posible[2][i] = (x == 0)? true: false;
                    for(int w = 0; w < 2; w++) {
                        posible[3][i] = (w == 0)? true: false;
                        for(int v = 0; v < 2; v++) {
                            posible[4][i] = (v == 0)? true: false;
                            for(int u = 0; u < 2; u++) {
                                posible[5][i] = (u == 0)? true: false;
                                for(int t = 0; t < 2; t++) {
                                    posible[6][i] = (t == 0)? true: false;
                                    for(int s = 0; s < 2; s++) {
                                        posible[7][i] = (s == 0)? true: false;
                                        for(int r = 0; r < 2; r++) {
                                            posible[8][i] = (r == 0)? true: false;
                                            for(int q = 0; q < 2; q++) {
                                                posible[9][i] = (q == 0)? true: false;
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
	    this.posible = posible;
        }
    }

    /**
     * Método que resuelve el problema 3SAT en base a todas las posibles
     * soluciones generadas por generatePosibleSolutions
     * @return Si es satisfacible
     */
    public boolean solve() {
        for(int i = 0; i < combinations; i++) {
            boolean s = solver(posible[0][i],posible[1][i],posible[2][i],
                        posible[3][i],posible[4][i],posible[5][i],posible[6][i],
                        posible[7][i],posible[8][i],posible[9][i]);
            if(s) {
            System.out.println();
                for(int j = 0; j < posible.length; j++) {
                    if((j+1) % 5 == 0) {
                        System.out.println(varnames[j] + "=" + posible[j][i] + "\t");
                    } else {
                        System.out.print(varnames[j] + "=" + posible[j][i] + "\t");
                    }
                }
                return true;
            }
        }
        return false;
    }

    /* Fase verificadora */
    /**
     * Metodo que indica si un ejemplar de posible solucion en verdad es solucion
     * @param z Valor de la 1 variable
     * @param y Valor de la 2 variable
     * @param x Valor de la 3 variable
     * @param w Valor de la 4 variable
     * @param v Valor de la 5 variable
     * @param u Valor de la 6 variable
     * @param t Valor de la 7 variable
     * @param s Valor de la 8 variable
     * @param r Valor de la 9 variable
     * @param q Valor de la 10 variable
     * @return Si la posible solucion satisface la expresion
     */
    private boolean solver(boolean z,boolean y,boolean x,boolean w,boolean v,
                           boolean u,boolean t,boolean s,boolean r,boolean q) {
        expresion.setValueTo(varnames[0],z);
        expresion.setValueTo(varnames[1],y);
        expresion.setValueTo(varnames[2],x);
        expresion.setValueTo(varnames[3],w);
        expresion.setValueTo(varnames[4],v);
        expresion.setValueTo(varnames[5],u);
        expresion.setValueTo(varnames[6],t);
        expresion.setValueTo(varnames[7],s);
        expresion.setValueTo(varnames[8],r);
        expresion.setValueTo(varnames[9],q);
        return expresion.solve();
    }
}
