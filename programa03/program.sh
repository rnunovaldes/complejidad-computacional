#!/bin/bash
# include this boilerplate
function jumpto {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

function pause {
 read -s -n 1 -p "Presione alguna tecla para continuar . . ."
 echo ""
}

start=${1:-"start"}

jumpto $start

start:
set -- $(locale LC_MESSAGES)
yesexpr="$1"; noexpr="$2"; yesword="$3"; noword="$4"

EXEC=main.Principal
JAVA_PRIN=./src/main/Principal.java
CLAS_PRIN=./bin/main/Principal.class
CLAS_UTIL1=./bin/utilidad/Reader.class
CLAS_UTIL2=./bin/utilidad/Printer.class
CLAS_UTIL3=./bin/utilidad/StringValidator.class
CLAS_UTIL4=./bin/utilidad/MenuConsole.class
CLAS_UTIL5=./bin/utilidad/console/SpecialCharacter.class
CLAS_UTIL6=./bin/utilidad/console/Color.class
CLAS_VIEW=./bin/mvc/view/Menu.class
CLAS_CON1=./bin/mvc/controller/WindowManager.class
CLAS_MOD1=./bin/mvc/model/Model.class
CLAS_MOD2=./bin/mvc/model/Chess.class
CLAS_MOD3=./bin/mvc/model/ChessPiece.class
CLAS_MOD4=./bin/mvc/model/Queen.class
CLAS_MOD5=./bin/mvc/model/ModelH.class
CLAS_MOD6=./bin/mvc/model/ModelSA.class
CLAS_MOD7=./bin/mvc/model/ModelTabu.class
CLAS_MOD8=./bin/mvc/model/ModelTabu\$Mov.class

while true; do
  read -p "¿Compilar? (${yesword} / ${noword})? " yn
  if [[ "$yn" =~ $yesexpr ]]; then jumpto comp; fi
  if [[ "$yn" =~ $noexpr ]]; then jumpto cont; fi
  echo "Answer ${yesword} / ${noword}."
done

cont:
if [[ -e "$CLAS_PRIN" ]] && [[ -e "$CLAS_UTIL1" ]] && [[ -e "$CLAS_UTIL2" ]]; then
if [[ -e "$CLAS_UTIL3" ]] && [[ -e "$CLAS_UTIL4" ]] && [[ -e "$CLAS_UTIL5" ]]; then
if [[ -e "$CLAS_UTIL6" ]] && [[ -e "$CLAS_VIEW" ]] && [[ -e "$CLAS_CON1" ]]; then
if [[ -e "$CLAS_MOD1" ]] && [[ -e "$CLAS_MOD2" ]] && [[ -e "$CLAS_MOD3" ]]; then
if [[ -e "$CLAS_MOD4" ]] && [[ -e "$CLAS_MOD5" ]] && [[ -e "$CLAS_MOD6" ]]; then
if [[ -e "$CLAS_MOD7" ]] && [[ -e "$CLAS_MOD8" ]]; then
jumpto exe; fi fi fi fi fi fi

comp:
if [[ ! -d ./bin ]]; then mkdir bin; fi
echo Vamos a compilar
javac -encoding UTF-8 -d bin -cp ..:src "$JAVA_PRIN"

exe:
echo Vamos a ejecutar el compilado
java -cp bin "$EXEC"
pause
echo Se eliminará la carpeta con los class
rm -rI ./bin
exit
