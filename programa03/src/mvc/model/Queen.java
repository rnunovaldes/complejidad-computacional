package mvc.model;

/**
 * Clase que modela una Reina de ajedrez
 */
public class Queen extends ChessPiece {

    /**
     * Constructor de la clase
     */
    public Queen() {
        label = 'Q';
        canJump = false;
    }

    /**
     * Verifica si una nueva posición para la pieza puede ser lograda con los
     * movimientos que tiene definidos
     *
     * @param newRow Fila de la nueva posición
     * @param newRow Columna de la nueva posición
     * @param isCapturing Esta pieza no tiene movimientos especiales al capturar
     *                    piezas, así que no importa el valor de este boleano
     * @return Un boleano si indica si es posible el movimiento
     */
    public boolean isValidMove(int currentRow, int currentCol, int newRow,
                                            int newCol, boolean isCapturing) {
        int rowDistance = Math.abs(newRow - currentRow);
        int colDistance = Math.abs(newCol - currentCol);
        // Movimiento en la misma fila/columna  || Movimiento en diagonal
        if(rowDistance == 0 || colDistance == 0 || rowDistance == colDistance) {
            return true;
        }
        return false;
    }

    /**
     * Método que clona la reina (una copia profunda)
     *
     * @return Un objeto que puede convertirse en ChessPiece
     * @throws No arroja excepciones
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return ((Object) new Queen());
    }
}
