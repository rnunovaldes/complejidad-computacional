package mvc.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 * Clase que se encarga de generar ejemplares de los problemas y sus soluciones
 * según recocido simulado
 */
public class ModelSA implements ModelH {

    private final Chess board;
    private final double TMIN = 1.0, COOLRATE = 0.88, P = 0.4;
    private int N = 1000;
    private double T = 800;
    private int[] solution;
    private Set<Chess> solutions = new HashSet<Chess>();
    private Random random = new Random();

    /**
     * Contructor de la clase
     *
     * @param board Un tablero de ajedrez vacío
     */
    public ModelSA(Chess board) {
        this.board = board;
        this.random = new Random();
        generateRandomInitialState();
    }

    /**
     * Resuleve el problema de las n-reinas con recocido simulado
     *
     * @return Cuántas soluciones distintas se obtuvieron
     */
    @Override
    public int solve() {
        // Tablero y solucion actual (configuración inicial)
        Chess cBoard = board;
        int[] cSolution = solution;
        int currentCollisions = howManyCollisions(cBoard, cSolution);
        // Si el estado inicial ya es una solución
        if(currentCollisions == 0) solutions.add(cBoard);
        // Iteraciones hasta condición de parada (se enfrió)
        int forceBreak = 0;
        while(T >= TMIN) {
            // Iteraciones para determinar los mejores valores
            for(int h = 0; h < N; h++) {
                // Permutación al azar de dos reinas
                int[] newSolution = generateNewSolution(cSolution);
                Chess newBoard;
                try {
                    newBoard = (Chess)cBoard.clone();
                } catch(Exception e) {
                    newBoard = new Chess(cBoard.getSize());
                }
                int count = 0;
                for(int i = 0; i < newSolution.length; i++){
                    if(cSolution[i] != newSolution[i]) {
                        newBoard.removePiece(i,cSolution[i]);
                        newBoard.setPiece('q',i,newSolution[i]);
                        count++;
                    }
                    if(count == 2) break;
                }
                currentCollisions = howManyCollisions(cBoard, cSolution);
                int newCollisions = howManyCollisions(newBoard, newSolution);
                // Si el nuevo estado ya es una solución
                if(newCollisions == 0) solutions.add(newBoard);
                int collisionsDiff = newCollisions - currentCollisions;
                // Checando si se tiene una mejor solución o si se permite una peor
                if(collisionsDiff <= 0 || acceptanceProbability(collisionsDiff)) {
                    cBoard = newBoard;
                    cSolution = newSolution;
                }
            }
            // Actualización de tempreatura y cantidad de iteraciones para mejorar valores
            T *= COOLRATE;
            N *= P;
        }
        return solutions.size();
    }

    /**
     * Da las soluciones obtenidas por el recocido simulado en un iterador con
     * formato de cadena
     *
     * @return Las soluciones en un iterador
     */
    @Override
    public Iterator<String> solutionsToIteratorString() {
        HashSet<String> stringSet = new HashSet<>();
        for (Chess b: solutions) {
            String string = b.toString();
            stringSet.add(string);
        }
        return stringSet.iterator();
    }

    /**
     * Da la representación en cadena del tablero
     *
     * @return La representación en cadena
     */
    @Override
    public String representateBoard() {
        return board.toString();
    }

    /**
     * Crea un estado inicial aleatoreo para el tablero.
     * Asigna a solution un arreglo de que tienen enteros de 0 a n-1 colocados
     * sin repetir y desordenados
     */
    private void generateRandomInitialState() {
        solution = new int[board.getSize()];
        for (int i = 0; i < board.getSize(); i++) {
            solution[i] = i;
        }
        Random random = new Random();
        for (int i = 0; i < board.getSize(); i++) {
            int j = random.nextInt(board.getSize() - i) + i;
            int temp = solution[i];
            solution[i] = solution[j];
            solution[j] = temp;
        }
        for(int i = 0; i < solution.length; i++) {
            board.setPiece('q',i,solution[i]);
        }
    }

    /**
     * Crea una nueva solución (vecino) basandose en una solución dada. Solo
     * cambia de lugar a dos reinas aleatoreas
     *
     * @param solution La solución base a la que se le moveran las dos reinas
     * @return Una nueva solución al mover dos reinas
     */
    private int[] generateNewSolution(int[] solution) {
        int size = solution.length;
        int[] neighbor = new int[solution.length];
        if(size == 1) {
            neighbor[0] = solution[0];
        } else {
            System.arraycopy(solution, 0, neighbor, 0, solution.length);
            int index1 = random.nextInt(size);
            int index2 = random.nextInt(size);
            while (index1 == index2) {
                index2 = random.nextInt(board.getSize());
            }
            int temp = solution[index1];
            neighbor[index1] = solution[index2];
            neighbor[index2] = temp;
        }
        return neighbor;
    }

    /**
     * Encuentra todas las colisiones que tienen las reinas en el tablero de
     * ajedrez
     *
     * @param board Un tablero con las reinas dispuestas en él
     * @param solution La posición de las reinas para que se puedan encontrar
     *                 sin iterar todo el tablero
     * @return El número de colisiones que tienen todas las reinas entre ellas
     */
    private static int howManyCollisions(Chess board, int[] solution) {
        int collisions = 0;
        for(int i = 0; i < board.getSize(); i++) {
            for(int j = i+1; j < board.getSize(); j++) {
                if(board.canCapture(i,solution[i], j,solution[j])) collisions++;
            }
        }
        return collisions;
    }

    /**
     * Indica si se va a aceptar una solución que es peor para poder explorar
     * varias soluciones. Si la solución es mejor siempre la acepta
     *
     * @param collisionDiff Un numero que indica si la solución es pero o mejor
     *                      Negativo es mejor, positivo es peor
     * @return Indica si se acepta la solución
     */
    private boolean acceptanceProbability(int collisionsDiff) {
        if (collisionsDiff < 0) {
            return true; // Si el vecino es mejor, siempre se acepta
        }
        return Math.exp(-collisionsDiff / T) > random.nextDouble();
    }
}
