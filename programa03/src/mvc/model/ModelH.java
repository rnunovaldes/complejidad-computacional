package mvc.model;

import java.util.Iterator;

/**
 * Clase que se encarga de generar ejemplares de los problemas y sus soluciones
 * según una metaheurística
 */
public interface ModelH {

    /**
     * Resuleve el problema de las n-reinas con la metaheurística
     *
     * @return Cuántas soluciones distintas se obtuvieron
     */
    public abstract int solve();

    /**
     * Da la solución obtenida en formato de cadena
     *
     * @return El iterador con las soluciones en forma de cadena
     */
    public abstract Iterator<String> solutionsToIteratorString();

    /**
     * Da la representación en cadena del tablero
     *
     * @return El iterador con las soluciones en forma de cadena
     */
    public abstract String representateBoard();
}
