package mvc.model;

/**
 * Clase que modela una pieza de ajedrez
 */
public abstract class ChessPiece {

    protected char label;
    protected boolean canJump;

    /**
     * Verifica si una nueva posición para la pieza puede ser lograda con los
     * movimientos que tiene definidos
     *
     * @param newRow Fila de la nueva posición
     * @param newRow Columna de la nueva posición
     * @param isCapturing Si el movimiento es de captura de pieza
     * @return Un boleano si indica si es posible el movimiento
     */
    public abstract boolean isValidMove(int currentRow, int currentCol,
                                int newRow, int newCol, boolean isCapturing);

    /**
     * Indica si la pieza puede saltarse piezas en medio de su camino
     *
     * @return
     */
    public boolean canJump() {
        return canJump;
    }

    /**
     * Compara si dos piezas son iguales
     *
     * @param obj Un objeto a comparar
     * @return Si son dos piezas iguales
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return true;
    }

    /**
     * Valor numérico generado a partir de atibutos en la clase
     *
     * @return Un entero, el cual es igual entre obj1.hashCode() y
     *         obj2.hashCode() si obj1.equals(obj2) es true
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int hashCode = 17;
        hashCode = prime * hashCode + (int) label;
        hashCode = prime * hashCode + (canJump ? 1 : 0);
        return hashCode;
    }

    /**
     * Método que clona la pieza (una copia profunda)
     *
     * @return Un objeto que puede convertirse en ChessPiece
     * @throws No arroja excepciones
     */
    @Override
    public abstract Object clone() throws CloneNotSupportedException;

    /**
     * Regresa una representación en cadena de la pieza de ajedrez
     *
     * @return La cadena
     */
    @Override
    public String toString() {
        return String.valueOf(label);
    }
}
