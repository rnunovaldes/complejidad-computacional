package mvc.model;

import utilidad.console.Color;

/**
 * Clase que modela un tablero de ajedrez y tiene métodos para colocar y retirar
 * piezas de ajedrez, checar movimientos de estas y otros
 */
public class Chess implements Cloneable {

    private int size;
    private ChessPiece[][] board;

    /**
     * Constructor de la clase
     */
    public Chess(int size) {
        if(size > 0) {
            this.size = size;
            board = new ChessPiece[size][size];
        } else {
            throw new IllegalArgumentException("El valor debe ser mayor a cero");
        }
    }

    /**
     * Contructor privado usado para clonar
     *
     * @param board El tablero de ajedrez que se usará
     */
    private Chess(ChessPiece[][] board) {
        this.board = board;
        this.size = board.length;
    }

    /**
     * Coloca una pieza en la posición indicada
     *
     * @param type El tipo de pieza que es ('q': REINA)
     * @param row La fila en la que se coloca
     * @param col La columna en la que se coloca
     * @return Si la pieza pudo ser colocada
     */
    public boolean setPiece(char type, int row, int col) {
        if(row < 0 || row >= size || col < 0 || col >= size) return false;
        switch(type) {
            case 'q': board[row][col] = new Queen();
                      break;
            default:
                throw new IllegalArgumentException("No se ha registrado ese tipo de pieza");
        }
        return true;
    }

    /**
     * Quita una pieza en la posición indicada
     *
     * @param row La fila en la que está la pieza a retirar
     * @param col La columna en la que está la pieza a retirar
     * @return Si la pieza pudo ser retirada
     */
    public boolean removePiece(int row, int col) {
        if(row < 0 || row >= size || col < 0 || col >= size) return false;
        if(board[row][col] != null) {
            board[row][col] = null;
            return true;
        }
        return false;
    }

    /**
     * Se encarga de validar un movimiento
     *
     * @param startRow La fila desde la cual se toma la pieza a efectuar
     *                 movimientos
     * @param startCol La columna desde la cual se toma la pieza a efectuar
     *                 movimientos
     * @param endRow La fila del movimiento objetivo
     * @param endCol La columna del movimiento objetivo
     */
    public boolean isValidMove(int startRow, int startCol, int endRow, int endCol) {
        if(startRow < 0 || startRow >= size || startCol < 0 || startCol >= size
           || endRow < 0 || endRow >= size || endCol < 0 || endCol >= size) {
            return false;
        }
        ChessPiece piece = board[startRow][startCol];
        if(piece == null) return false;
        boolean ocupied = board[endRow][endCol] == null;
        if(!piece.canJump()
           && arePicesBetween(startRow, startCol, endRow, endCol)) return false;
        return piece.isValidMove(startRow, startCol, endRow, endCol, ocupied);
    }

    /**
     * Verifica si hay piezas en medio del camino desde una posicion inicial a
     * una final
     *
     * @param startRow La fila desde la cual se toma la pieza a efectuar
     *                 movimientos
     * @param startCol La columna desde la cual se toma la pieza a efectuar
     *                 movimientos
     * @param endRow La fila del movimiento objetivo
     * @param endCol La columna del movimiento objetivo
     * @return Si hay piezas en medio del camino
     */
    private boolean arePicesBetween(int startRow, int startCol, int endRow, int endCol) {
        int rowDistance = Math.abs(endRow - startRow);
        int colDistance = Math.abs(endCol - startCol);
        if (startRow == endRow) {
            int colIncrement = (endCol > startCol)? 1 : -1;
            for(int col = startCol + colIncrement; col != endCol; col += colIncrement) {
                if(board[startRow][col] != null) return true;
            }
        } else if(startCol == endCol) {
            int rowIncrement = (endRow > startRow) ? 1 : -1;
            for(int row = startRow + rowIncrement; row != endRow; row += rowIncrement) {
                if(board[row][startCol] != null) return true;
            }
        } else if (rowDistance == colDistance) {
            int rowIncrement = (endRow > startRow) ? 1 : -1;
            int colIncrement = (endCol > startCol) ? 1 : -1;
            int row = startRow + rowIncrement;
            int col = startCol + colIncrement;
            while (row != endRow && col != endCol) {
                if(board[row][col] != null) return true;
                row += rowIncrement;
                col += colIncrement;
            }
        }
        return false;
    }

    /**
     * Indica si una pieza en la posición indicada puede o no capturar otra
     * pieza en cualquier lugar del tablero
     *
     * @param row La fila de la pieza a comprobar si puede o no capturar otras
     *            piezas
     * @param col La columna de la pieza a comprobar si puede o no capturar
     *            otras piezas
     * @return Si hay una pieza en la posición indicada y esta puede capturar
     *         al menos otra pieza en el tablero
     */
    public boolean canCaptureAll(int row, int col) {
        if(row < 0 || row >= size || col < 0 || col >= size) return false;
        if(board[row][col] == null) return false;
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                if(row == i && col == j) continue;
                if(this.isValidMove(row, col, i, j)) {
                    if(board[i][j] != null) return true;
                }
            }
        }
        return false;
    }

    /**
     * Indica si una pieza en la posición indicada puede o no capturar otra
     * pieza en otra posición indicada del tablero
     *
     * @param row La fila de la pieza que captura
     * @param col La columna de la pieza que captura
     * @param capRow La fila de la pieza a comprobar si puede ser capturada por
     *               la otra pieza
     * @param capCol La columna de la pieza a comprobar si puede ser capturada
     *               por la otra pieza
     * @return Si hay piezas en las posiciones indicadas y la primera puede
     *         capturar a la segunda
     */
    public boolean canCapture(int row, int col, int capRow, int capCol) {
        if(this.isValidMove(row, col, capRow, capCol)) {
            if(board[capRow][capCol] != null) return true;
        }
        return false;
    }

    /**
     * Compara si dos tableros son iguales
     *
     * @param obj Un objeto a comparar
     * @return Si las dos mismas reinas está involucradas en el movimiento
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Chess other = (Chess)obj;
        if(size != other.getSize()) return false;
        ChessPiece[][] otherBoard = other.getBoard();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                ChessPiece obj1 = board[i][j];
                ChessPiece obj2 = otherBoard[i][j];
                if (obj1 == null && obj2 == null) continue;
                if (obj1 == null || obj2 == null) return false;
                if (!obj1.equals(obj2)) return false;
            }
        }
        return true;
    }

    /**
     * Valor numérico generado a partir de atibutos en la clase
     *
     * @return Un entero, el cual es igual entre obj1.hashCode() y
     *         obj2.hashCode() si obj1.equals(obj2) es true
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int hashCode = 17;
        hashCode = prime * hashCode + size;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                ChessPiece piece = board[i][j];
                hashCode = prime * hashCode + (piece != null ? piece.hashCode() : 0);
            }
        }
        return hashCode;
    }

    /**
     * Método que clona el tablero (una copia profunda)
     *
     * @return Un objeto que puede convertirse en Chess
     * @throws No arroja excepciones
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return (Object) new Chess(getBoard());
    }

    /**
     * Regresa una representación en cadena del tablero de juego
     * Ejemplo de 4x4 con dos reinas:
     *      ╔───╤───╗
     *      │ Q |   │
     *      ╟───╬───╣
     *      │   │ Q │
     *      ╚───╧───╝
     *
     * @return Una representación
     */
    @Override
    public String toString() {
        String str = "╔─" + ("──╦─").repeat(size-1) + "──╗\n";
        for(int row = 0; row < size; row++) {
            str += "│";
            for(int col = 0; col < size; col++) {
                ChessPiece piece = board[row][col];
                str += (piece == null)? "   │": " " + piece.toString() + " │";
            }
            str += "\n";
            if(row+1 != size) {
                str += "╠─" + ("──╬─").repeat(size-1) + "─" + "─╣" + "\n";
            }
        }
        str += "╚─" + ("──╩─").repeat(size-1) + "──╝";
        return str;
    }

    /**
     * Obtiene el tamaño del tablero de ajedrez
     *
     * @return Un entero que es el tamaño n del tablero de nxn
     */
    public int getSize() {
        return size;
    }

    /**
     * Obtiene el una copia del tablero ajedrez para ser usada fuera de la clase
     *
     * @return Una copia profunda del tablero
     */
    public ChessPiece[][] getBoard() {
        ChessPiece[][] clonedBoard = new ChessPiece[size][size];
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board.length; j++) {
                if(board[i][j] != null) {
                    try {
                        clonedBoard[i][j] = (ChessPiece)board[i][j].clone();
                    } catch(Exception e) {}
                }

            }
        }
        return clonedBoard;
    }
}
