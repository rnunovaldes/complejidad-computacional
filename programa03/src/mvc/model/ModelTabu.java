package mvc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 * Clase que se encarga de pedir ejemplares de los problemas y sus soluciones
 * según búsqueda tabú
 */
public class ModelTabu implements ModelH {

    /**
     * Clase privada que representa los movimientos de las reinas junto con el
     * tablero resultante y su ubicación
     */
    private class Mov implements Comparable<Mov> {
        private int r1, r2, collisions;
        private Chess board;
        private int[] solution;

        /**
         * Constuctor de la clase
         *
         * @param r1 Fila de una reina a intercambiar lugar
         * @param r2 Fila de la otra reina a intercambiar lugar
         * @param board El tablero de ajedrez resultante del movimiento de las
         *              dos reinas
         * @param solution La ubicación de todas las reinas ya aplicado el
         *                 movimiento de las dos escogidas
         */
        public Mov(int r1, int r2, Chess board, int[] solution) {
            this.r1 = r1;
            this.r2 = r2;
            this.board = board;
            this.solution = solution;
            this.collisions = ModelTabu.howManyCollisions(board, solution);
        }

        /**
         * La primera reina que fue afectada por el movimieto
         *
         * @return Un entero que es el número de la reina afectada. Colocarlo en
         *         solution dará su ubicación en la columna
         */
        public int getR1() {
            return r1;
        }

        /**
         * La segunda reina que fue afectada por el movimiento
         *
         * @return Un entero que es el número de la reina afectada. Colocarlo en
         *         solution dará su ubicación en la columna
         */
        public int getR2() {
            return r2;
        }

        /**
         * Obtiene el tablero asignado a este movimiento
         *
         * @return El tablero con el movimiento aplicado
         */
        public Chess getBoard() {
            return board;
        }

        /**
         * Obtiene la solución que son las ubicaciones de las reinas en el
         * tablero
         *
         * @return La ubicación de las reinas
         */
        public int[] getSolution() {
            return solution;
        }

        /**
         * Obtiene la cantidad de colisiones que hay en el tablero
         *
         * @return Cuantas parejas de reinas pueden capturarse mutuamente en el
         *         tablero asignado a este movimiento
         */
        public int getCollisions() {
            return collisions;
        }

        /**
         * Compara dos movimietos basándose en el numero de colisiones que
         * tienen
         *
         * @param other Un movimiento a comparar
         * @return Un entero que es menor a 0 si quien llama el metodo es menor;
         *         mayor a 0 si other es menor; 0 si son iguales
         */
        @Override
        public int compareTo(Mov other) {
            return Integer.compare(this.collisions, other.collisions);
        }

        /**
         * Compara si dos movimientos son de las dos mismas reinas
         *
         * @param obj Un objeto a comparar
         * @return Si las dos mismas reinas está involucradas en el movimiento
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Mov other = (Mov)obj;
            return r1 == other.r1 && r2 == other.r2;
        }

        /**
         * Representación en cadena de los movimientos
         *
         * @return Una cadena que tiene el número de las reinas movidas y el
         *         número de coliciones resultantes de ese movimiento
         */
        @Override
        public String toString() {
            return "("+r1+","+r2+") Col:"+collisions;
        }
    }

    private final Chess board;
    private final int MAXITER = 100, LIMIT = 50, TABULONG = 5;
    private final Mov[] tabuList = new Mov[TABULONG];
    private Chess cBoard;
    private int[] solution, cSolution, FREQUENCY;
    private Set<Chess> solutions = new HashSet<Chess>();

    /**
     * Constructor de la clase
     *
     * @param board Un tablero de ajedrez vacío
     */
    public ModelTabu(Chess board) {
        this.board = board;
        generateRandomInitialState();
    }

    /**
     * Resuleve el problema de las n-reinas con búsqueda tabú
     *
     * @return Si terminó de resolver el problema con un estado exitoso
     */
    @Override
    public int solve() {
        int currentCollitions = howManyCollisions(board, solution);
        if(currentCollitions == 0) {
            solutions.add(board);
        }
        // Tablero y solucion actual (configuración inicial)
        cBoard = board;
        cSolution = solution;
        int combinations = (int)calculateCombinations(solution.length, 2);
        FREQUENCY = new int[combinations];
        // Iteraciones de la búsqueda tabú
        for(int iter = 0; iter < MAXITER; iter++) {
            bestPermutation(combinations, iter);
        }
        return solutions.size();
    }

    /**
     * Da las soluciones obtenidas por la búsqueda tabú en un iterador con
     * formato de cadena
     *
     * @return Las soluciones en un iterador
     */
    @Override
    public Iterator<String> solutionsToIteratorString() {
        HashSet<String> stringSet = new HashSet<>();
        for (Chess b: solutions) {
            String string = b.toString();
            stringSet.add(string);
        }
        return stringSet.iterator();
    }

    /**
     * Da la representación en cadena del tablero
     *
     * @return La representación en cadena
     */
    @Override
    public String representateBoard() {
        return board.toString();
    }

    /**
     * Método miscelaneo. Encuentra las combinaciones posibles de elementos
     * nCk = n! / (k! * (n - k)!)
     *
     * @param n Número de elementos en el conjunto
     * @param k Tamaño de las combinaciones
     * @return El número de combinaciones posibles
     */
    private static long calculateCombinations(int n, int k) {
        long numerator = factorial(n);
        long denominator = factorial(k) * factorial(n - k);
        return numerator / denominator;
    }

    /**
     * Método miscelaneo. Calcula un número factorial
     *
     * @param n Número a conseguir su factorial
     * @return El resultado del factorial
     */
    private static long factorial(int n) {
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    /**
     * Coloca un nuevo elemento en la lista tabú. Usar este método pues siempre
     * lo pone en tabuList[0], pero antres recorre todos los elementos que ya
     * tiene hacia la derecha (dando como resultado la eliminación del último)
     *
     * @param mov El movimiento que se guardará para no volver a realizarlo
     */
    private void setTabu(Mov mov) {
        for(int i = TABULONG-1; i > 0; i--) {
            tabuList[i] = tabuList[i-1];
        }
        tabuList[0]= mov;
    }

    /**
     * Crea un estado inicial aleatoreo para el tablero.
     * Asigna a solution un arreglo de que tienen enteros de 0 a n-1 colocados
     * sin repetir y desordenados
     */
    private void generateRandomInitialState() {
        solution = new int[board.getSize()];
        for (int i = 0; i < board.getSize(); i++) {
            solution[i] = i;
        }
        Random random = new Random();
        for (int i = 0; i < board.getSize(); i++) {
            int j = random.nextInt(board.getSize() - i) + i;
            int temp = solution[i];
            solution[i] = solution[j];
            solution[j] = temp;
        }
        for(int i = 0; i < solution.length; i++) {
            board.setPiece('q',i ,solution[i]);
        }
    }

    /**
     * Encuentra todas las colisiones que tienen las reinas en el tablero de
     * ajedrez
     *
     * @param board Un tablero con las reinas dispuestas en él
     * @param solution La posición de las reinas para que se puedan encontrar
     *                 sin iterar todo el tablero
     * @return El número de colisiones que tienen todas las reinas entre ellas
     */
    private static int howManyCollisions(Chess board, int[] solution) {
        int collisions = 0;
        for(int i = 0; i < board.getSize(); i++) {
            for(int j = i+1; j < board.getSize(); j++) {
                if(board.canCapture(i,solution[i], j,solution[j])) collisions++;
            }
        }
        return collisions;
    }

    /**
     * Escoge un movimiento de reinas que tenga pocas coliciones, no sea tabú y
     * no sea muy frecuente dado un tablero de ajedrez y las posiciones de las
     * reinas
     *
     * @param combinations Cuantas combinaciones posibles se espera que tengan
     *                     los movimientos de las reinas
     * @param iter En qué iteración va. Usado para saber cuando tomar en cuenta
     *             la memoria de frecuencias
     */
    private void bestPermutation(int combinations, int iter) {
        // Crear permutaciones de las reinas
        int k = 0;
        Mov[] posiblePermutations = new Mov[combinations];
        for(int i = 0; i < cBoard.getSize(); i++) {
            for(int j = i+1; j < cBoard.getSize(); j++) {
                int[] permutedSolution = new int[cSolution.length];
                System.arraycopy(cSolution, 0, permutedSolution, 0, cSolution.length);
                int temp = cSolution[i];
                permutedSolution[i] = cSolution[j];
                permutedSolution[j] = temp;
                Chess cloneBoard;
                try {
                    cloneBoard = (Chess)cBoard.clone();
                } catch(Exception e) {
                    cloneBoard = new Chess(cBoard.getSize());
                }
                cloneBoard.removePiece(i,cSolution[i]);
                cloneBoard.removePiece(j,cSolution[j]);
                cloneBoard.setPiece('q',i,permutedSolution[i]);
                cloneBoard.setPiece('q',j,permutedSolution[j]);
                posiblePermutations[k++] = new Mov(i,j,cloneBoard,permutedSolution);
            }
        }
        // Ordenar para tomar los casos más probables
        Mov[] sortedPermutations = new Mov[posiblePermutations.length];
        System.arraycopy(posiblePermutations,0,sortedPermutations,0,posiblePermutations.length);
        Arrays.sort(sortedPermutations);
        ArrayList<Integer> indexes = new ArrayList<>(posiblePermutations.length);
        for(Mov mov: sortedPermutations) {
            for(int i = 0; i < posiblePermutations.length; i++) {
                if(posiblePermutations[i].equals(mov)) {
                    indexes.add(i);
                    break;
                }
            }
        }
        // Encontrar frecuencia más alta
        int maxFrequency = -1;
        for(int frequency: FREQUENCY) {
            if(frequency > maxFrequency) maxFrequency = frequency;
        }
        // Tomar una permutación de reinas y continuar
        for(int i = 0; i < sortedPermutations.length; i++) {
            Mov checking = sortedPermutations[i];
            // Se encontró una solución de 0 colisiones. Guardar y siguiente
            if(checking.getCollisions() == 0 && i != sortedPermutations.length-1) {
                solutions.add(checking.getBoard());
                continue;
            }
            // Tomar permutación de pocas colisiones, no tabú y no frecuente
            boolean isTabu = false;
            for(Mov pt: tabuList) {
                try {
                    // Es tabú, checar siguiente
                    if(checking.equals(pt)) {
                        isTabu = true;
                        break;
                    }
                // La lista tabú es nula en uno de sus elementos
                // (solo primeras 3 iteraciones)
                } catch(Exception e) {
                    break;
                }
            }
            // Si no es tabú, tomar como solución
            if(!isTabu && i != sortedPermutations.length-1) {
                // Si es frecuetne, prohibir solución y buscar otra
                if(iter >= LIMIT && FREQUENCY[indexes.get(i)] >= maxFrequency
                   && i != sortedPermutations.length-1) {
                    continue;
                }
                cBoard = checking.getBoard();
                cSolution = checking.getSolution();
                FREQUENCY[indexes.get(i)] = FREQUENCY[indexes.get(i)]+1;
                setTabu(checking);
                break;
            }
        }
    }
}
