package mvc.model;

import java.util.Iterator;

import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir ejemplares de los problemas y sus soluciones
 */
public class Model {
    private WindowManager wm;
    private ModelH mh;

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las ventanas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que genera un tablero de ajedrez en un estado inicial para
     * la búsqueda tabú
     *
     * @param size Cuántas filas y columnas tendrá el tablero de ajedrez
     * @return Una representación del tablero en cadena
     */
    public String generateBoardForTabu(int size){
        mh = new ModelTabu(new Chess(size));
        return mh.representateBoard();
    }

    /**
     * Método que genera un tablero de ajedrez en un estado inicial para el
     * recocido simulado
     *
     * @param size Cuántas filas y columnas tendrá el tablero de ajedrez
     * @return Una representación del tablero en cadena
     */
    public String generateBoardForAnnealing(int size){
        mh = new ModelSA(new Chess(size));
        return mh.representateBoard();
    }

    /**
     * Da la representación en cadena del tablero
     *
     * @return La representación en cadena
     */
    public String representateBoard() {
        return mh.representateBoard();
    }

    /**
     * Indica que se debe resolver el problema con la metaheurística que le
     * corresponda
     *
     * @return Cuántas soluciones distintas se obtuvieron
     */
    public int solve() {
        return mh.solve();
    }

    /**
     * Indica que se genere un iterador con la representación en cadena de las
     * soluciones obtenidas
     *
     * @return El iterador con las soluciones en forma de cadena
     */
    public Iterator<String> visualizateSolutions() {
        return mh.solutionsToIteratorString();
    }
}
