package mvc.view;

import java.util.Iterator;

import utilidad.Printer;
import utilidad.MenuConsole;
import mvc.controller.WindowManager;

/**
 * Clase para mostrar menu con opciones en la consola
 */
public class Menu {

    private static final String[] OPTIONS =
    {/*0*/"¿Qué tamaño tiene el tablero cuadrado? (Solo ponga un número)",
     /*1*/"¿Acepta este ejemplar para solucionarlo?",
     /*2*/"¿Quiere volver a usar el programa o salir?",
     /*3*/"No use cero (0)",
     /*4*/"¿Generar otro ejemplar con esta misma metaheurística?",
     /*5*/"Generar otro ejemplar",/*6*/"Salir",
     /*7*/"¿Con qué metaheurística se solucionará el problema?",
     /*8*/"Búsqueda Tabú", /*9*/"Recocido Simulado",
     /*10*/"Visualizando solución #%d (Hay %d más)",
     /*11*/"¿Ver siguiete solución?",
     /*12*/"No se encontraron soluciones",
     /*13*/"No hay más soluciones que mostrar"};
    private MenuConsole cm = new MenuConsole();
    private WindowManager wm;
    private int solutions = 0;

    public Menu(WindowManager wm) {
        this.wm = wm;
    }

    /**
     * Método que inicia el menu para imprimirse en consola
     */
    public void start() {
        boolean loop = true;
        while(loop) {
            if(cm.askDual(OPTIONS[7], OPTIONS[8], OPTIONS[9])) {
                solveByTabu();
            } else {
                solveByAnnealing();
            }
            showSolutions();
            loop = cm.askDual(OPTIONS[2], OPTIONS[5], OPTIONS[6]);
        }
    }

    /**
     * Inicializar el tablero con parametros válidos para búsqueda tabú
     */
    private void startBoardForTabu() {
        boolean loop = true;
        while(loop) {
            Printer.printWarning(OPTIONS[0]);
            try {
                Printer.println(wm.requestBoardForTabu(cm.askInteger(false)));
                loop = false;
            } catch(IllegalArgumentException e) {
                Printer.printError(OPTIONS[3]);
            }
        }
    }

    /**
     * Menú para resolver el problema con búsqueda tabú
     */
    private void solveByTabu() {
        boolean loop = true;
        while(loop) {
            startBoardForTabu();
            if(cm.askYesNo(OPTIONS[1])) {
                solutions = wm.requestSolution();
                loop = false;
            } else {
                loop = cm.askYesNo(OPTIONS[4]);
            }
        }
    }

    /**
     * Inicializar el tablero con parametros válidos para recocido simulado
     */
    private void startBoardForGenetic() {
        boolean loop = true;
        while(loop) {
            Printer.printWarning(OPTIONS[0]);
            try {
                Printer.println(wm.requestBoardForAnnealing(cm.askInteger(false)));
                loop = false;
            } catch(IllegalArgumentException e) {
                Printer.printError(OPTIONS[3]);
            }
        }
    }

    /**
     * Menú para resolver el problema con recocido simulado
     */
    private void solveByAnnealing() {
        boolean loop = true;
        while(loop) {
            startBoardForGenetic();
            if(cm.askYesNo(OPTIONS[1])) {
                solutions = wm.requestSolution();
                loop = false;
            } else {
                loop = cm.askYesNo(OPTIONS[4]);
            }
        }
    }

    /**
     * Menú para mostar soluciones
     */
    private void showSolutions() {
        boolean loop = true;
        Iterator it = wm.requestSolutionView();
        int count = 1;
        if(solutions == 0) {
            Printer.printError(OPTIONS[12]);
            return;
        }
        while(loop) {
            Printer.printExtra(String.format(OPTIONS[10],count,solutions-count));
            count++;
            Printer.println((String)it.next());
            if(!it.hasNext()) {
                Printer.printError(OPTIONS[13]);
                loop = false;
            } else {
                loop = cm.askYesNo(OPTIONS[11]);
            }
        }
    }
}
