package mvc.controller;

import java.util.Iterator;

import mvc.view.Menu;
import mvc.model.Model;

/**
 * Clase que coordina las ventanas y la lógica del proyecto
 */
public class WindowManager {
    private Menu menu;
    private Model model;

    /**
	 * Constructor de la clase
	 */
	public WindowManager() {
	    menu = new Menu(this);
		model = new Model(this);
        menu.start();
	}

    /**
     * Método que solicita una representación en cadena del tablero
     *
     * @return La representación en cadena
     */
    public String requestBoardRepresentation() {
        return model.representateBoard();
    }

    /**
     * Método para avisar al controlador que debe generar un tablero nuevo para
     * solucionarlo con búsqueda tabú
     *
     * @param elements El tamaño del tablero
     * @return La representación en cadena del tablero
     */
    public String requestBoardForTabu(int size) {
        return model.generateBoardForTabu(size);
    }

    /**
     * Método para avisar al controlador que debe generar un tablero nuevo para
     * solucionarlo con recocido simulado
     *
     * @param elements El tamaño del tablero
     * @return La representación en cadena del tablero
     */
    public String requestBoardForAnnealing(int size) {
        return model.generateBoardForAnnealing(size);
    }

    /**
     * Solicita solución del problema
     *
     * @return Cuántas soluciones distintas se obtuvieron
     */
    public int requestSolution() {
        return model.solve();
    }

    /**
     * Solicita un tirador con la representación en cadena de las soluciones
     * obtenidas
     *
     * @return El iterador con las soluciones en cadena
     */
    public Iterator<String> requestSolutionView() {
        return model.visualizateSolutions();
    }
}
